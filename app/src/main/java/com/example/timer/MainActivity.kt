package com.example.timer

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.viewpager2.widget.ViewPager2
import com.example.timer.databinding.ActivityMainBinding
import com.example.timer.view.adapter.TimerViewPagerAdapter
import com.example.timer.view.ui.AlertMainFragment
import com.example.timer.view.ui.TimerFragment
import com.example.timer.view.ui.UiStateInterface
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var currAlertState = 0
    private var currTimerState = 0
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        loadBackground()
        val adapter = TimerViewPagerAdapter(this)
        adapter.setAlertStateInterface(object : UiStateInterface {
            override fun setState(state: Int) {
                currAlertState = state
                changeButtons(binding.pagerMain.currentItem)
            }
        })
        adapter.setTimerStateInterface(object : UiStateInterface {
            override fun setState(state: Int) {
                currTimerState = state
                changeButtons(binding.pagerMain.currentItem)
            }
        })


        binding.pagerMain.adapter = adapter
        TabLayoutMediator(binding.tabLayoutMain, binding.pagerMain) { tab, position ->
            when(position) {
                0 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_access_time_24)
                1 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_outline_timer_24)
                2 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_notifications_24)
            }
        }.attach()

        binding.pagerMain.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                changeButtons(position)
            }
        })

        binding.actionButtonLeft.setOnClickListener { (binding.pagerMain.adapter as TimerViewPagerAdapter).pressLeftButton(binding.pagerMain.currentItem)}
        binding.actionButtonCenter.setOnClickListener { (binding.pagerMain.adapter as TimerViewPagerAdapter).pressCenterButton(binding.pagerMain.currentItem)}
        binding.actionButtonRight.setOnClickListener { (binding.pagerMain.adapter as TimerViewPagerAdapter).pressRightButton(binding.pagerMain.currentItem)}
        setContentView(binding.root)
    }

    private fun changeButtons(tab: Int) {
        when(tab) {
            0 -> {
                binding.actionButtonLeft.visibility = View.GONE
                binding.actionButtonCenter.visibility = View.GONE
                binding.actionButtonRight.visibility = View.GONE
            }
            1 -> {
                binding.actionButtonLeft.visibility = View.GONE
                binding.actionButtonCenter.visibility = View.VISIBLE
                binding.actionButtonRight.visibility = View.GONE
                when(currTimerState) {
                    TimerFragment.STATE_RUNNING -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_replay_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_stop_24))
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                    TimerFragment.STATE_STOPPED -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_replay_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_play_circle_24))
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                    else -> {
                        binding.actionButtonLeft.visibility = View.GONE
                        binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_play_circle_24))
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                }
            }
            2 -> {
                binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_play_circle_24))
                when (currAlertState) {
                    AlertMainFragment.ALERT_MAIN_FRAGMENT_STOPPED -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_delete_outline_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_play_circle_24))
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.VISIBLE
                    }
                    AlertMainFragment.ALERT_MAIN_FRAGMENT_PLAYING -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_outline_delete_outline_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_stop_24))
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.VISIBLE
                    }
                    AlertMainFragment.ALERT_NEW_FRAGMENT -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_close_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                    AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY -> {
                        binding.actionButtonLeft.visibility = View.GONE
                        binding.actionButtonCenter.visibility = View.GONE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                    AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY_READY -> {
                        binding.actionButtonLeft.visibility = View.GONE
                        binding.actionButtonCenter.visibility = View.VISIBLE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                    else -> {
                        binding.actionButtonLeft.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_baseline_close_24))
                        binding.actionButtonLeft.visibility = View.VISIBLE
                        binding.actionButtonCenter.visibility = View.GONE
                        binding.actionButtonRight.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let {
                    binding.root.background = BitmapDrawable(resources, it)
                }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {  }
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {  }
        }

        Picasso.get().load("http://195.201.125.8/TimerApp/back.png").into(target)
    }
}