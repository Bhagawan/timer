package com.example.timer.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.timer.view.ui.*

class TimerViewPagerAdapter (fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
    private val clock = ClockFragment()
    private val timer = TimerFragment()
    private val alert = AlertMainFragment()

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> clock
        1 -> timer
        else -> alert
    }

    fun setAlertStateInterface(i: UiStateInterface) {
        alert.setInterface(i)
    }

    fun setTimerStateInterface(i: UiStateInterface) {
        timer.setInterface(i)
    }

    fun pressLeftButton(page: Int) {
        if(page == 1) timer.leftButton()
        else if(page == 2) alert.leftButton()
    }

    fun pressCenterButton(page: Int) {
        if(page == 1) timer.centerButton()
        else if(page == 2) alert.centerButton()
    }

    fun pressRightButton(page: Int) {
        if(page == 1) timer.rightButton()
        else if(page == 2) alert.rightButton()
    }

    fun getPage(page: Int) : Fragment = when(page) {
        0 -> clock
        1 -> timer
        else -> alert
    }
}