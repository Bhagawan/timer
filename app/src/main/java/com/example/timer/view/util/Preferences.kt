package com.example.timer.view.util

import android.content.Context
import android.content.SharedPreferences

class Preferences {
    companion object {
        fun saveAlert(context: Context, alertTime: Int) {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            val currentAlerts = shP.getStringSet("alerts", emptySet())
            val newSet = currentAlerts?.toMutableSet() ?: HashSet<String>()
            newSet.add(alertTime.toString())
            shP.edit().putStringSet("alerts", newSet).apply()
        }

        fun getAlerts(context: Context) : List<Int> {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            val currentAlerts = shP.getStringSet("alerts", emptySet())

            return currentAlerts?.map { it.toIntOrNull() ?: 0 } ?: emptyList()
        }

        fun deleteAlert(context: Context, alertTime: Int) {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            val currentAlerts = shP.getStringSet("alerts", emptySet())
            val newSet = currentAlerts?.toMutableSet() ?: HashSet<String>()
            newSet.remove(alertTime.toString())
            shP.edit().putStringSet("alerts", newSet).apply()
        }

        fun getSportInfo(context: Context): SportInfo? {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            val height = shP.getInt("height", 0)
            val weight = shP.getInt("weight", 0)
            val distance = shP.getInt("distance", 0)
            val time = shP.getInt("time", 0)
            return if(height == 0 || weight == 0 || distance == 0 || time == 0) null
            else SportInfo(height, weight, distance, time)
        }

        fun saveSportInfo(context: Context, height: Int, weight: Int, distance: Int, time: Int) {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            shP.edit().putInt("height", height).putInt("weight", weight).putInt("distance", distance).putInt("time", time).apply()
        }

        fun saveLastAttempt(context: Context, time: String) {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            shP.edit().putString("lastAttempt", time).apply()
        }

        fun getLastAttempt(context: Context): String? {
            val shP: SharedPreferences =
                context.getSharedPreferences("savedSettings", Context.MODE_PRIVATE)
            return shP.getString("lastAttempt", "")
        }

    }
}