package com.example.timer.view.viewmodel

import android.os.Build
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.timer.view.util.TimerServerClient
import com.example.timer.view.util.TimerSplashResponse
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SplashViewModel(simLanguage: String): ViewModel() {
    private var requestInProcess = true

    private val mutableFlow = MutableSharedFlow<Boolean>(1, 1, BufferOverflow.DROP_OLDEST)
    val outputFlow = mutableFlow.asSharedFlow()

    val url = MutableLiveData("")
    var logoUrl = "http://195.201.125.8/TimerApp/logo.png"

    val logoVisibility = MutableLiveData(true)

    init {
        showLogo()
        TimerServerClient.create().getSplash(Locale.getDefault().language, simLanguage, Build.MODEL, TimeZone.getDefault().displayName.replace("GMT", ""))
            .enqueue(object : Callback<TimerSplashResponse> {
                override fun onResponse(
                    call: Call<TimerSplashResponse>,
                    response: Response<TimerSplashResponse>
                ) {
                    if (requestInProcess) {
                        requestInProcess = false
                        hideLogo()
                    }
                    response.body()?.let {
                        if(it.url != "no") {
                            url.postValue("https://${it.url}")
                        } else switchToMain()
                    } ?: switchToMain()
                }

                override fun onFailure(call: Call<TimerSplashResponse>, t: Throwable) {
                    switchToMain()
                }
            })
    }

    private fun showLogo() {
        if(requestInProcess) {
           logoVisibility.postValue(true)
        }
    }

    private fun switchToMain() {
        mutableFlow.tryEmit(true)
    }

    fun hideLogo() {
        logoVisibility.postValue(false)
    }

}