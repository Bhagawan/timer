package com.example.timer.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.example.timer.databinding.FragmentTimerBinding
import com.example.timer.view.util.Preferences
import com.example.timer.view.viewmodel.TimerViewModel

class TimerFragment : Fragment(), ActionButtonsInterface {
    private lateinit var binding: FragmentTimerBinding
    private val viewModel = TimerViewModel()
    private var stateInterface: UiStateInterface? = null

    companion object {
        const val STATE_RUNNING = 0
        const val STATE_STOPPED = 1
        const val STATE_STOPPED_DEFAULT = 2
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTimerBinding.inflate(layoutInflater)

        val info = context?.let {
            Preferences.getSportInfo(it)
        }
        val s = info?.distance.toString() + "м"
        binding.textInfoTimeIdeal.text = s
        val last = context?.let { Preferences.getLastAttempt(it) }
        if(!last.isNullOrBlank()) {
            binding.timerLast.visibility = View.VISIBLE
            binding.textInfoTimeLast.text = last
        } else {
            binding.timerLast.visibility = View.GONE
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.timerData.collect { binding.textTimer.text = it}
        }

        viewModel.vState.observe(viewLifecycleOwner) { state ->
            stateInterface?.setState(if(viewModel.playState.value == true) STATE_RUNNING
            else if(state == true) STATE_STOPPED
            else STATE_STOPPED_DEFAULT
            )
        }

        viewModel.playState.observe(viewLifecycleOwner) { state ->
            stateInterface?.setState(if(state) STATE_RUNNING
            else if(viewModel.vState.value == true) STATE_STOPPED
            else STATE_STOPPED_DEFAULT
            )
        }

        viewModel.lastAttempt.observe(viewLifecycleOwner) {
            if(it.isNotEmpty()) {
                context?.let { it1 -> Preferences.saveLastAttempt(it1, it) }
                binding.timerLast.visibility = View.VISIBLE
                binding.textInfoTimeLast.text = it
            } else {
                binding.timerLast.visibility = View.GONE
            }
        }

        return binding.root
    }

    fun setInterface(i: UiStateInterface) {
        stateInterface = i
        i.setState(if(viewModel.playState.value == true) STATE_RUNNING
        else if(viewModel.vState.value == true) STATE_STOPPED
        else STATE_STOPPED_DEFAULT)
    }


    override fun leftButton() {
        viewModel.restartTimer()
    }

    override fun centerButton() {
        viewModel.play()
    }

    override fun rightButton() {
    }

}