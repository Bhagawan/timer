package com.example.timer.view.ui

interface UiStateInterface {
    fun setState(state: Int)
}