package com.example.timer.view.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.timer.view.ui.AlertItemFragment
import com.example.timer.view.ui.UiStateInterface

class AlertPagerAdapter (fragment: Fragment) : FragmentStateAdapter(fragment) {
    private val list = ArrayList<Fragment>()

    override fun getItemCount(): Int = list.size

    override fun createFragment(position: Int): Fragment = when (position) {
        in list.indices -> list[position]
        else -> Fragment()
    }

    fun addAlert(time: Int, i: UiStateInterface) {
        val f = AlertItemFragment(time)
        f.setInterface(i)
        list.add(f)
    }

    fun get(position: Int) : Fragment {
        return list[position]
    }

    fun clear() = list.clear()

    fun removeAlert(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }
}