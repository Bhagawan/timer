package com.example.timer.view.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class TimerViewModel: ViewModel() {
    private val timerCoroutineScope = CoroutineScope(Dispatchers.Default)
    private var timer: Job? = null
    private var timerPause = false

    val vState = MutableLiveData(false)
    val playState = MutableLiveData(false)

    private var overall = ""
    val lastAttempt = MutableLiveData<String>()

    private val mutableTimerData = MutableStateFlow("00:00:00")
    val timerData = mutableTimerData.asStateFlow()

    init {
        timerCoroutineScope.launch {
            mutableTimerData.emit("00:00:00")
            playState.postValue(false)
            vState.postValue(false)
        }
    }

    fun play() {
        if(timer != null) {
            timerPause = !timerPause
            vState.postValue(timerPause)
            playState.postValue(!timerPause)
        } else {
            playState.postValue(true)
            overall = ""
            timer = timerCoroutineScope.launch {
                var time = 0
                while(isActive) {
                    if(!timerPause) {

                        val millisecond = if((time / 10)  % 100 < 10) "0${(time / 10)  % 100}" else ((time / 10)  % 100).toString()
                        val sec = if((time / 1000) % 60 < 10) "0${(time / 1000) % 60}" else ((time / 1000) % 60).toString()
                        val min = if((time / 1000) / 60 < 10) "0${(time / 1000) / 60}" else ((time / 1000) / 60).toString()
                        overall = "$min:$sec:$millisecond"
                        mutableTimerData.emit("$min:$sec:$millisecond")
                        time++
                    }
                    delay(10)
                }
            }
        }
    }

    fun restartTimer() {
        timer?.let {
            it.cancel()
            timer = null
            lastAttempt.postValue(overall)
            overall = ""
        }
        timerCoroutineScope.launch {
            mutableTimerData.emit("00:00:00")
        }
        timerPause = false
        vState.postValue(timerPause)
        playState.postValue(timerPause)
    }
}