package com.example.timer.view.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlin.math.absoluteValue

class AlertItemViewModel:ViewModel() {
    private val mutableTimeString = MutableStateFlow("00:00:00")
    val timeFlow = mutableTimeString.asStateFlow()

    private val mutableProgressBr = MutableStateFlow(0)
    val progressBar = mutableProgressBr.asStateFlow()

    val alarm = MutableLiveData(false)
    val playButtonState = MutableLiveData(false)

    private var time = 0
    private var currTime = 0
    private var pause = false

    private var timer: Job? = null

    fun setTime(time: Int) {
        this.time = time
        showTime(time)
    }

    fun play() {
        if(timer != null) {
            pause = !pause
            alarm.postValue(false)
            playButtonState.postValue(false)
        } else {
            startTimer()
            playButtonState.postValue(true)
        }
    }

    fun restart() {
        timer?.cancel()
        timer = null
        showTime( time )
        CoroutineScope(Dispatchers.Default).launch { mutableProgressBr.emit(0) }
        alarm.postValue(false)
        playButtonState.postValue(false)
    }

    private fun showTime(t: Int) {
        var str = if(t < 0) "-"
        else ""
        val s = t.absoluteValue % 100
        val min = (t.absoluteValue / 100) % 100
        val hours = (t.absoluteValue / 10000) % 100

        str += if(hours < 10) "0${hours}:" else "${hours}:"
        str += if(min < 10) "0${min}:" else "${min}:"
        str += if(s < 10) "0$s" else "$s"
        CoroutineScope(Dispatchers.Default).launch { mutableTimeString.emit(str) }
    }

    private fun startTimer() {
        currTime = time
        pause = false
        timer = CoroutineScope(Dispatchers.Default).launch {
            while(isActive) {
                if(!pause) {
                    currTime--
                    showTime(currTime)
                    mutableProgressBr.emit((currTime / (time / 100.0f)).toInt())
                    if(currTime < 0) alarm.postValue(true)
                }
                delay(1000)
            }
        }
    }
}