package com.example.timer.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.timer.databinding.FragmentAlertNewBinding
import com.example.timer.view.util.Preferences
import com.example.timer.view.viewmodel.AlertNewViewModel

class AlertNewFragment : Fragment(), ActionButtonsInterface {
    private lateinit var binding: FragmentAlertNewBinding
    private val viewModel = AlertNewViewModel()
    private var stateInterface: UiStateInterface? = null
    private var only : Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAlertNewBinding.inflate(layoutInflater)

        viewModel.timeString.observe(viewLifecycleOwner) {
            binding.alertAddTime.text = it
        }

        viewModel.playButtonState.observe(viewLifecycleOwner) {
            val state = if(only) {
                if(it) AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY_READY
                else  AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY
            } else {
                if(it) AlertMainFragment.ALERT_NEW_FRAGMENT
                else  AlertMainFragment.ALERT_NEW_FRAGMENT_EMPTY
            }
            stateInterface?.setState(state)
        }

        context?.let { only = Preferences.getAlerts(it).isEmpty() }
        binding.btnAlertAdd0.setOnClickListener { viewModel.typeNumber(0) }
        binding.btnAlertAdd1.setOnClickListener { viewModel.typeNumber(1) }
        binding.btnAlertAdd2.setOnClickListener { viewModel.typeNumber(2) }
        binding.btnAlertAdd3.setOnClickListener { viewModel.typeNumber(3) }
        binding.btnAlertAdd4.setOnClickListener { viewModel.typeNumber(4) }
        binding.btnAlertAdd5.setOnClickListener { viewModel.typeNumber(5) }
        binding.btnAlertAdd6.setOnClickListener { viewModel.typeNumber(6) }
        binding.btnAlertAdd7.setOnClickListener { viewModel.typeNumber(7) }
        binding.btnAlertAdd8.setOnClickListener { viewModel.typeNumber(8) }
        binding.btnAlertAdd9.setOnClickListener { viewModel.typeNumber(9) }
        binding.btnAlertAddBackspace.setOnClickListener { viewModel.deleteNumber() }

        return binding.root
    }

    fun setInterface(i: UiStateInterface) {
        stateInterface = i
        val state = if(only) {
            if(viewModel.playButtonState.value == true) AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY_READY
            else  AlertMainFragment.ALERT_NEW_FRAGMENT_ONLY
        } else {
            if(viewModel.playButtonState.value == true) AlertMainFragment.ALERT_NEW_FRAGMENT
            else  AlertMainFragment.ALERT_NEW_FRAGMENT_EMPTY
        }
        i.setState(state)
    }

    override fun leftButton() {
        parentFragmentManager.popBackStack()
    }

    override fun centerButton() {
        context?.let { it1 -> Preferences.saveAlert(it1, viewModel.getTime()) }
        parentFragmentManager.popBackStack()
    }

    override fun rightButton() { }

}