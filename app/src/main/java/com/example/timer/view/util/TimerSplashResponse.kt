package com.example.timer.view.util

import androidx.annotation.Keep

@Keep
class TimerSplashResponse(val url : String)