package com.example.timer.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.timer.R
import com.example.timer.databinding.FragmentAlertMainBinding

class AlertMainFragment : Fragment(), ActionButtonsInterface {
    private lateinit var binding: FragmentAlertMainBinding
    private var stateInterface: UiStateInterface? = null

    companion object {
        const val ALERT_MAIN_FRAGMENT_PLAYING = 5
        const val ALERT_MAIN_FRAGMENT_STOPPED = 0
        const val ALERT_NEW_FRAGMENT = 1
        const val ALERT_NEW_FRAGMENT_EMPTY = 2
        const val ALERT_NEW_FRAGMENT_ONLY = 3
        const val ALERT_NEW_FRAGMENT_ONLY_READY = 4
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAlertMainBinding.inflate(layoutInflater)

        childFragmentManager.addFragmentOnAttachListener { _, fragment ->
            when(fragment::class.java) {
                AlertFragment::class.java -> {
                    (fragment as AlertFragment).setInterface(object : UiStateInterface {
                        override fun setState(state: Int) {
                            stateInterface?.setState(state)
                        }
                    })
                    stateInterface?.setState(ALERT_MAIN_FRAGMENT_STOPPED)
                }
                AlertNewFragment::class.java -> {
                    (fragment as AlertNewFragment).setInterface(object : UiStateInterface {
                        override fun setState(state: Int) {
                            stateInterface?.setState(state)
                        }
                    })
                    stateInterface?.setState(ALERT_NEW_FRAGMENT)
                }
            }
        }
        childFragmentManager.beginTransaction().replace(R.id.fragmentContainerView, AlertFragment()).commit()
        return binding.root
    }

    fun setInterface(i: UiStateInterface) {
        stateInterface = i
        i.setState(if(getCurrentFragment() is AlertNewFragment) ALERT_NEW_FRAGMENT else ALERT_MAIN_FRAGMENT_STOPPED)
    }

    override fun leftButton() {
        val f = getCurrentFragment()
        if(f is ActionButtonsInterface) f.leftButton()
    }

    override fun centerButton() {
        val f = getCurrentFragment()
        if(f is ActionButtonsInterface) f.centerButton()
    }

    override fun rightButton() {
        val f = getCurrentFragment()
        if(f is ActionButtonsInterface) f.rightButton()
    }

    private fun getCurrentFragment() : Fragment? {
        if(isAdded) {
            val fragments = childFragmentManager.fragments
            for(f in fragments) if (f.isVisible) return  f
        }
        return null
    }
}