package com.example.timer.view.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.content.res.AppCompatResources
import com.example.timer.R
import com.example.timer.databinding.FragmentAlertBinding
import com.example.timer.view.adapter.AlertPagerAdapter
import com.example.timer.view.util.Preferences
import com.google.android.material.tabs.TabLayoutMediator

class AlertFragment : Fragment(), ActionButtonsInterface {
    private lateinit var binding : FragmentAlertBinding
    private var stateInterface: UiStateInterface? = null
    private var mediator: TabLayoutMediator? = null
    private var currPage :Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAlertBinding.inflate(layoutInflater)
        binding.pagerAlert.isSaveEnabled = false

        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()
        context?.let {
            binding.pagerAlert.adapter = AlertPagerAdapter(this)
            (binding.pagerAlert.adapter as AlertPagerAdapter).clear()
            val alerts =  Preferences.getAlerts(it)
            if(alerts.isEmpty()) addAlert(false)
            for(a in alerts) {
                (binding.pagerAlert.adapter as AlertPagerAdapter).addAlert(a, object :UiStateInterface {
                    override fun setState(state: Int) {
                        stateInterface?.setState(state)
                    }
                })
            }
            mediator?.detach()
            mediator = TabLayoutMediator(binding.tabLayoutAlert, binding.pagerAlert) { _, _ ->
                AppCompatResources.getDrawable(requireContext(), R.drawable.dot_selector)
            }
            mediator?.attach()
            (binding.pagerAlert.adapter as AlertPagerAdapter).notifyDataSetChanged()
            val p = (binding.pagerAlert.adapter as AlertPagerAdapter).itemCount - 1
            currPage?.let { page ->
                binding.pagerAlert.setCurrentItem(if(page < p) page else p, false)
                currPage = null
            }
        }
    }

    override fun onPause() {
        super.onPause()
        currPage = binding.pagerAlert.currentItem
    }

    private fun addAlert(effect: Boolean) {
        if(effect)
            parentFragmentManager.beginTransaction().setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.anim.anim_in, R.anim.anim_out).replace(R.id.fragmentContainerView, AlertNewFragment()).addToBackStack("").commit()
        else parentFragmentManager.beginTransaction().replace(R.id.fragmentContainerView, AlertNewFragment()).addToBackStack("").commit()
    }

    override fun leftButton() {
        val c = (binding.pagerAlert.adapter as AlertPagerAdapter).get(binding.pagerAlert.currentItem)
        //(binding.pagerAlert.adapter as AlertPagerAdapter).removeAlert(binding.pagerAlert.currentItem)
        if(c is AlertItemFragment) {
            context?.let { it1 -> Preferences.deleteAlert(it1, c.getTime()) }
        }
        onResume()
    }

    override fun centerButton() {
        val c = (binding.pagerAlert.adapter as AlertPagerAdapter).get(binding.pagerAlert.currentItem)
        if(c is ActionButtonsInterface) {
            c.centerButton()
        }
    }

    override fun rightButton() {
        addAlert(true)
    }

    fun setInterface(i: UiStateInterface) {
        stateInterface = i
        i.setState(AlertMainFragment.ALERT_MAIN_FRAGMENT_STOPPED)
    }
}