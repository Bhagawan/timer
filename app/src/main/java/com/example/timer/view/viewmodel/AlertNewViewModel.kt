package com.example.timer.view.viewmodel

import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AlertNewViewModel: ViewModel() {
    val timeString = MutableLiveData<SpannableString>()
    val playButtonState = MutableLiveData(false)
    private var time = 0

    init {
        showTime()
    }

    fun typeNumber(number: Int) {
        if(time == 0) time = number else {
            time *= 10
            time += number
        }
        playButtonState.postValue(true)
        showTime()
    }

    fun deleteNumber() {
        time /= 10
        showTime()
        if(time == 0) playButtonState.postValue(false)
    }

    private fun showTime() {
        val s = time % 100
        val min = (time / 100) % 100
        val hours = (time / 10000) % 100

        var str = if(hours < 10) "0${hours}ч" else "${hours}ч"
        str+= if(min < 10) "0${min}м" else "${min}м"
        str+= if(s < 10) "0${s}с" else "${s}м"

        val spanS = SpannableString(str)
        spanS.setSpan(RelativeSizeSpan(0.5f), 2,3,0)
        spanS.setSpan(RelativeSizeSpan(0.5f), 5,6,0)
        spanS.setSpan(RelativeSizeSpan(0.5f), 8,9,0)
        timeString.postValue(spanS)
    }

    fun getTime() : Int {
        return time
    }
}