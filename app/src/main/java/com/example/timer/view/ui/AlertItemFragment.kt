package com.example.timer.view.ui

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.example.timer.R
import com.example.timer.databinding.FragmentAlertItemBinding
import com.example.timer.view.viewmodel.AlertItemViewModel

class AlertItemFragment(private val time: Int) : Fragment(), ActionButtonsInterface {
    private lateinit var binding: FragmentAlertItemBinding
    private val viewModel = AlertItemViewModel()
    private lateinit var player : MediaPlayer
    private var stateInterface: UiStateInterface? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.setTime(time)
        binding = FragmentAlertItemBinding.inflate(layoutInflater)

        binding.btnAlertItemRestart.setOnClickListener { viewModel.restart() }

        lifecycleScope.launchWhenCreated {
            viewModel.timeFlow.collect { binding.textAlertTime.text = it}
        }
        lifecycleScope.launchWhenCreated {
            viewModel.progressBar.collect{
                binding.progressBarAlert.progress = it
            }
        }

        player = MediaPlayer.create(binding.root.context, R.raw.alarm2 )
        player.isLooping = true
        player.setVolume(100.0f,100.0f)

        viewModel.alarm.observe(viewLifecycleOwner) {
            if(it) {
                if(!player.isPlaying) {
                    player.start()
                }
            } else {
                if(player.isPlaying) {
                    player.pause()
                }
            }
        }

        viewModel.playButtonState.observe(viewLifecycleOwner) { state ->
            stateInterface?.setState(if(state) AlertMainFragment.ALERT_MAIN_FRAGMENT_PLAYING else AlertMainFragment.ALERT_MAIN_FRAGMENT_STOPPED)
        }
        return binding.root
    }

    override fun leftButton() { }

    override fun centerButton() {
        viewModel.play()
    }

    override fun rightButton() { }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
    }

    fun setInterface(i: UiStateInterface) {
        stateInterface = i
        i.setState(if(viewModel.playButtonState.value == true) AlertMainFragment.ALERT_MAIN_FRAGMENT_PLAYING else AlertMainFragment.ALERT_MAIN_FRAGMENT_STOPPED)
    }

    fun getTime() : Int {
        return time
    }

}