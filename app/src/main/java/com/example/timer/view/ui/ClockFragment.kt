package com.example.timer.view.ui

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.timer.R
import com.example.timer.databinding.AlertInputBinding
import com.example.timer.databinding.FragmentClockBinding
import com.example.timer.view.util.Preferences
import com.example.timer.view.util.SportInfo
import com.example.timer.view.viewmodel.ClockViewModel

class ClockFragment : Fragment() {
    private lateinit var binding: FragmentClockBinding
    private val viewModel = ClockViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentClockBinding.inflate(layoutInflater)
        lifecycleScope.launchWhenCreated {
            viewModel.timeFlow.collect { binding.textClock.text = it}
        }
        viewModel.day.observe(viewLifecycleOwner) {day ->
            binding.textClockDate.text = day
        }

        context?.let {
            val info = Preferences.getSportInfo(it)
            if(info == null) showInputAlert(it, null)
            else fillInfo(info)
        }

        binding.layoutInfo.setOnClickListener {
            context?.let {
                showInputAlert(it, Preferences.getSportInfo(it))
            }
        }

        return binding.root
    }

    private fun fillInfo(info: SportInfo) {
        binding.textInfoHeight.text = info.height.toString()
        binding.textInfoWeight.text = info.weight.toString()
        binding.textInfoDistance.text = info.distance.toString()
        binding.textInfoTime.text = info.height.toString()
    }

    private fun showInputAlert(context: Context, info: SportInfo?) {
        val b = AlertInputBinding.inflate(layoutInflater)

        val dialog = object: AlertDialog(context,R.style.MyDialog) {
            override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
                if (ev.action == MotionEvent.ACTION_DOWN) {
                    val v = currentFocus
                    if (v is EditText) {
                        val outRect = Rect()
                        v.getGlobalVisibleRect(outRect)
                        if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                            v.clearFocus()
                            val imm = context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                        }
                    }
                }
                return super.dispatchTouchEvent(ev)
            }
        }
        dialog.setView(b.root)

        dialog.setCancelable(false)
        info?.let {
            b.textInputHeight.setText(it.height.toString())
            b.textInputWeight.setText(it.weight.toString())
            b.textInputDistance.setText(it.distance.toString())
            b.textInputTime.setText(it.height.toString())
        }

        b.btnInput.setOnClickListener {
            val height = b.textInputHeight.text.toString()
            val weight = b.textInputWeight.text.toString()
            val distance = b.textInputDistance.text.toString()
            val time = b.textInputTime.text.toString()
            if(height == "" || weight == "" || distance == "" || time == "") Toast.makeText(context, getString(R.string.alert_error), Toast.LENGTH_SHORT).show()
            else {
                Preferences.saveSportInfo(context, height.toInt(), weight.toInt(), distance.toInt(), time.toInt() )
                fillInfo(SportInfo(height.toInt(), weight.toInt(), distance.toInt(), time.toInt()))
                dialog.dismiss()
            }
        }

        dialog.show()
    }

}