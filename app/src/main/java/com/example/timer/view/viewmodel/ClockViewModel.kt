package com.example.timer.view.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.text.SimpleDateFormat
import java.util.*

class ClockViewModel: ViewModel() {

    private val mutableTimeFlow = MutableStateFlow("00:00:00")
    val timeFlow = mutableTimeFlow.asStateFlow()
    val day = MutableLiveData<String>()

    init {
        CoroutineScope(Dispatchers.Default).launch {
            day.postValue(SimpleDateFormat("EEE,MMM d", Locale("ru","RU")).format(Date()))
            while(isActive) {
                mutableTimeFlow.emit(SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date()))
                delay(1000)
            }
        }
    }


}