package com.example.timer.view.util

data class SportInfo(val height: Int, val weight: Int, val distance: Int, val time: Int)
