package com.example.timer.view.ui

interface ActionButtonsInterface {
    fun leftButton()
    fun centerButton()
    fun rightButton()
}